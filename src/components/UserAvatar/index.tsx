import React from "react";
import { Avatar, AvatarProps, Box, Grid, Typography } from "@mui/material";

interface UserAvatarProps extends AvatarProps {
  username: string;
  subtitle?: string;
}
export const UserAvatar = ({
  username,
  subtitle,
  ...restProps
}: UserAvatarProps) => {
  return (
    <Grid container direction={"column"}>
      <Grid item>
        <Grid container alignItems={"center"} spacing={1}>
          <Grid item>
            <Avatar {...restProps} />
          </Grid>
          <Grid item>
            <Typography variant="h6" className="header-message">
              {username}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        {subtitle && (
          <Box sx={{ display: "block" }}>
            <Typography variant="subtitle1" className="header-message">
              {subtitle}
            </Typography>
          </Box>
        )}
      </Grid>
    </Grid>
  );
};
