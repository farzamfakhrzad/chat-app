import * as React from "react";
import Snackbar from "@mui/material/Snackbar";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

export default function SimpleSnackbar({
  handleClose,
  message,
}: {
  handleClose: () => void;
  message?: string;
}) {
  const [open, setOpen] = React.useState(false);

  React.useEffect(() => {
    if (message != null) setOpen(true);
  }, [message]);

  const handleInternalClose = (
    _: React.SyntheticEvent | Event,
    reason?: string,
  ) => {
    if (reason === "clickaway") {
      return;
    }
    handleClose();
    setOpen(false);
  };

  const action = (
    <React.Fragment>
      <IconButton
        size="small"
        aria-label="close"
        color="inherit"
        onClick={handleClose}
      >
        <CloseIcon fontSize="small" />
      </IconButton>
    </React.Fragment>
  );

  return (
    <div>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={handleInternalClose}
        message={message}
        action={action}
      />
    </div>
  );
}
