import React from "react";
import wrap from "@/helpers/test";
import { faker } from "@faker-js/faker";
import SnackBar from "./index";

// Mock the handleClose function
const handleClose = jest.fn();

describe("SimpleSnackbar", () => {
  it("renders correctly with a message", () => {
    const message = faker.lorem.sentence(); // Generate a random sentence as the message
    const { asFragment } = wrap(
      <SnackBar handleClose={handleClose} message={message} />,
    );
    expect(asFragment()).toMatchSnapshot();
  });

  it("renders correctly without a message", () => {
    const { asFragment } = wrap(<SnackBar handleClose={handleClose} />);
    expect(asFragment()).toMatchSnapshot();
  });

  it("calls handleClose when closed", () => {
    const { getByLabelText } = wrap(
      <SnackBar handleClose={handleClose} message={faker.lorem.sentence()} />,
    );
    const closeButton = getByLabelText("close"); // Find the close button by its "aria-label" attribute
    closeButton.click(); // Simulate a click on the close button
    expect(handleClose).toHaveBeenCalled();
  });
});
