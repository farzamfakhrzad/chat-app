import { useContext, createContext } from "react";
import { GlobalStore } from "./store";

export const GlobalStoreContext = createContext<GlobalStore>(null!);
export function useGlobalStore(): GlobalStore {
  return useContext(GlobalStoreContext);
}
