import { callEndpoint, RequestState, State } from "@/modules/axios";
import { Instance, types } from "mobx-state-tree";
export const GlobalStore = types
  .model("GlobalStore", {
    notification: types.maybe(types.string),
    ping: types.optional(types.frozen<RequestState<string>>(), {
      state: State.Initial,
    }),
  })
  .actions((self) => ({
    setPing: (ping: RequestState<string>) => {
      self.ping = ping;
    },
    setNotification: (notification: string) => {
      self.notification = notification;
    },
    resetNotification: () => {
      self.notification = undefined;
    },
  }))
  .views((self) => ({
    isConnectedToServer: () => self.ping.state === State.Success,
  }))
  .actions((self) => ({
    pingServer: async () => {
      const request = callEndpoint<string>({
        config: {
          method: "GET",
          url: "/ping",
        },
        abort: self.ping.abort,
        store: self,
      });
      for await (const response of request) {
        self.setPing(response);
      }
    },
  }))
  .actions((self) => ({
    init() {
      self.pingServer();
    },
  }));
export type GlobalStore = Instance<typeof GlobalStore>;
