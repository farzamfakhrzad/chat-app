import React, { useCallback, useEffect, useMemo } from "react";
import { observer } from "mobx-react-lite";
import { Grid, Paper, Typography, useMediaQuery } from "@mui/material";
import { ChatList, ChatRoom } from "./screens";
import { UserAvatar } from "@/components/UserAvatar";
import { Conversation } from "./store";
import { useChatStore } from "./store/hooks";
import { useAuthenticationStore } from "../authentications/store/hooks";

function MessengerComponent() {
  const chatStore = useChatStore();
  const authenThicationStore = useAuthenticationStore();
  const isMobile = useMediaQuery("(max-width:600px)");
  const [conversations, setConversations] = React.useState<Conversation[]>([]);
  const selectedConversationId = useMemo(() => {
    return chatStore.selectedConversationId;
  }, [chatStore.selectedConversationId]);
  const user = useMemo(
    () => authenThicationStore.LogegdInUser?.data,
    [authenThicationStore.LogegdInUser.state],
  );
  if (!user) {
    throw new Error("User is not logged in");
  }
  useEffect(() => {
    chatStore.fetchConversation();
  }, []);
  useEffect(() => {
    if (chatStore.chats.data) setConversations(chatStore.chats.data);
  }, [chatStore.chats]);

  const getUserDataById = useCallback(
    (id: number) => authenThicationStore.searchUserById(id),
    [],
  );
  const onConversationClick = useCallback((id: number) => {
    chatStore.setSelectedConversationId(id);
  }, []);

  return (
    <Grid container direction={"column"} spacing={1}>
      <Grid item>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="h5" className="header-message">
              Chat
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid container component={Paper}>
          <Grid item xs={4}>
            <UserAvatar username={user.name} />
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        {/* Render the desktop version */}
        {!isMobile && (
          <Grid container component={Paper}>
            <Grid item xs={0} sm={4}>
              <ChatList
                conversations={conversations}
                userId={user.id}
                getUserDataById={getUserDataById}
                onConversationClick={onConversationClick}
                selectedConversationId={selectedConversationId}
              />
            </Grid>
            <Grid item xs={8}>
              <ChatRoom selectedConversationId={selectedConversationId} />
            </Grid>
          </Grid>
        )}

        {/* Render the mobile version */}

        {isMobile && selectedConversationId && (
          <ChatRoom selectedConversationId={selectedConversationId} />
        )}

        {isMobile && !selectedConversationId && (
          <ChatList
            conversations={conversations}
            userId={user.id}
            getUserDataById={getUserDataById}
            onConversationClick={onConversationClick}
            selectedConversationId={selectedConversationId}
          />
        )}
      </Grid>
    </Grid>
  );
}

export const Messenger = observer(MessengerComponent);
