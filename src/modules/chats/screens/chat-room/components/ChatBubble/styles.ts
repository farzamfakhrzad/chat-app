import styled from "@emotion/styled";
import { Paper } from "@mui/material";

export const StyledChatBubble = styled(Paper, {
  shouldForwardProp: (prop) => prop !== "isCurrentUserSender",
})<{ isCurrentUserSender: boolean }>`
  background-color: ${(props) =>
    props.isCurrentUserSender ? "#00aaff" : "#4caf50"};
  color: #fff;
  padding: 8px;
  border-radius: 16px;
  max-width: 70%;
  margin-bottom: 8px;
  align-self: ${(props) =>
    props.isCurrentUserSender ? "flex-end" : "flex-start"};
`;
