import { Message } from "@/modules/chats/store";
import { observer } from "mobx-react-lite";
import { Chip, Avatar, Typography } from "@mui/material";
import React from "react";
import { StyledChatBubble } from "./styles";
import { Box } from "@mui/system";

interface ChatBubbleProps {
  message: Message;
  userName?: string;
  currenUserID: number;
}

function ChatBubbleComponent({
  message,
  userName,
  currenUserID,
}: ChatBubbleProps) {
  const isCurrentUserSender = message?.userId === currenUserID;
  return (
    <StyledChatBubble isCurrentUserSender={isCurrentUserSender}>
      <Box sx={{ py: "0px !important" }}>
        <Typography>{message?.text}</Typography>
      </Box>
      <Box>
        {new Date(message?.sentAt).toLocaleString()}
        {userName && (
          <Chip
            sx={{ ml: "auto" }}
            avatar={<Avatar sx={{ width: 24, height: 24 }} />}
            label={userName}
          />
        )}
      </Box>
    </StyledChatBubble>
  );
}

export const ChatBubble = observer(ChatBubbleComponent);
