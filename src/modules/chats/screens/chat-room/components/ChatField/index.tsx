import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import { Message } from "@/modules/chats/store";
import { ChatBubble } from "../ChatBubble";
import { Container } from "./styles";

interface ChatFieldProps {
  messages: Message[] | [];
  userId: number;
  members: { id: number; name: string }[];
}
function ChatFieldCompoenent({ messages, members, userId }: ChatFieldProps) {
  const scrollableDivRef = React.useRef<HTMLDivElement>(null);
  useEffect(() => {
    const div = scrollableDivRef.current;
    if (div && div.scrollHeight > div.clientHeight) {
      div.scrollTop = div.scrollHeight - div.clientHeight;
    }
  }, [messages]);
  return (
    <Container ref={scrollableDivRef}>
      {messages ? (
        messages.map((message) => (
          <ChatBubble
            key={message.id}
            currenUserID={userId}
            message={message}
            userName={
              members.find((member) => member.id === message?.userId)?.name
            }
          />
        ))
      ) : (
        <div>NO MESSAGE</div>
      )}
    </Container>
  );
}

export const CHatField = observer(ChatFieldCompoenent);
