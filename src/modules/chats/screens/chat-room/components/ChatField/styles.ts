import styled from "@emotion/styled";
export const Container = styled("div")`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  overflow: auto;
  height: 70%;
`;
