import React, { useEffect, useMemo, useState } from "react";
import TextField from "@mui/material/TextField";
import SendIcon from "@mui/icons-material/Send";
import Box from "@mui/material/Box";
import { Grid, IconButton, Toolbar, useMediaQuery } from "@mui/material";
import { useChatStore } from "../../store/hooks";
import { observer } from "mobx-react-lite";
import { UserAvatar } from "@/components/UserAvatar";
import { CHatField } from "./components/ChatField";
import { useAuthenticationStore } from "@/modules/authentications/store/hooks";
import { ArrowBack } from "@mui/icons-material";
interface ChatRoomProps {
  selectedConversationId: number;
}
function ChatRoomComponent({ selectedConversationId }: ChatRoomProps) {
  const chatStore = useChatStore();
  const authenThicationStore = useAuthenticationStore();
  const isMobile = useMediaQuery("(max-width:600px)");

  const [inputText, setInputText] = useState("");

  useEffect(() => {
    if (selectedConversationId) {
      chatStore.getConversationInfo();
    }
  }, [selectedConversationId]);
  const handleSubmit = (message: string) => {
    chatStore.sendMesaage({ text: message });
  };
  const goToConversationList = () => {
    chatStore.setSelectedConversationId(0);
  };
  const messages = useMemo(() => {
    if (!chatStore.selectedConversationMessages?.data) return [];
    return chatStore.selectedConversationMessages.data;
  }, [chatStore.selectedConversationMessages]);
  const roomData = useMemo(() => {
    if (chatStore.selectedConversationData?.data)
      return chatStore.selectedConversationData.data;
  }, [chatStore.selectedConversationData]);
  const memberNames = useMemo(() => {
    if (roomData?.members)
      return roomData.members
        .filter((member) => member.id !== chatStore.loggedInUserId)
        .map((member) => {
          const memberObj = authenThicationStore.searchUserById(member.id);
          return { name: memberObj ? memberObj.name : "TEST", id: member.id };
        });
    return [];
  }, [roomData]);
  const userName = useMemo(() => {
    if (roomData)
      return roomData.name
        ? roomData.name
        : roomData.members?.find(
            (member) => member.id !== chatStore.loggedInUserId,
          )?.name;
  }, [roomData]);

  return selectedConversationId && roomData && memberNames.length > 0 ? (
    <div>
      <Box
        sx={{
          width: "100%",
          bgcolor: "background.paper",
          position: "relative",
          height: 450,
          overflow: "auto",
          padding: 1,
        }}
      >
        <Toolbar>
          {isMobile ? (
            <Grid
              container
              spacing={2}
              alignItems="center"
              justifyContent={"space-between"}
            >
              <Grid item>
                <IconButton aria-label="Back" onClick={goToConversationList}>
                  <ArrowBack />
                </IconButton>
              </Grid>
              <Grid item>
                <UserAvatar username={userName ?? "test"} />
              </Grid>
            </Grid>
          ) : (
            <UserAvatar
              username={userName ?? "test"}
              subtitle={
                memberNames.length > 1
                  ? memberNames.map((member) => member.name).join(",")
                  : ""
              }
            />
          )}
        </Toolbar>
        <CHatField
          messages={messages}
          userId={chatStore.loggedInUserId}
          members={memberNames}
        />
        <Box
          sx={{
            display: "flex",
            position: "absolute",
            bottom: 0,
            width: "100%",
          }}
          component="form"
        >
          <TextField
            margin="normal"
            required
            fullWidth
            name="message"
            onChange={(event) => {
              setInputText(event.target.value);
            }}
            value={inputText}
            placeholder="Type your message here"
            id="message"
          />
          <IconButton
            type="submit"
            sx={{ p: 2 }}
            onClick={() => {
              if (inputText) {
                // echo(testInput);
                handleSubmit(inputText);
                setInputText("");
              }
            }}
          >
            <SendIcon />
          </IconButton>
        </Box>
      </Box>
    </div>
  ) : (
    <div>No conversation selected</div>
  );
}

export const ChatRoom = observer(ChatRoomComponent);
