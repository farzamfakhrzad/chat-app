import React, { useCallback } from "react";
import { useAuthenticationStore } from "@/modules/authentications/store/hooks";
import { List } from "@mui/material";
import { observer } from "mobx-react-lite";
import { useChatStore } from "../../store/hooks";
import { Conversation, User } from "../../store";
import { ChatBanner } from "./components/conversationBanner";
import { ConversationCreatorDialog } from "./components/conversationCreatorDialog";
import { ConversationFab } from "./components/conversationFab";
import { StyledChatListContainer } from "./styles";

export interface Props {
  onConversationClick?: (index: number) => void;
  conversations?: Conversation[];
  selectedConversationId: number;
  onScrollToBottom?: () => void;
  mobileView?: boolean;
  userId: number;
  getUserDataById: (id: number) => User | undefined;
}

function ChatListComponent({ conversations, ...props }: Props) {
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const authenThicationStore = useAuthenticationStore();
  const chatStore = useChatStore();
  const createConversation = useCallback(
    ({ userIds, name }: { userIds: number[]; name?: string }) => {
      chatStore
        .createConversation({ user_ids: userIds, ...(name && { name }) })
        .then(() => {
          setDialogOpen(false);
          //chatStore.fetchConversation()
        });
    },
    [],
  );
  return (
    <>
      <StyledChatListContainer>
        <List
          sx={{
            width: "100%",
            bgcolor: "background.paper",
            height: 450,
            overflow: "auto",
            padding: 1,
          }}
        >
          {conversations && conversations.length > 0 ? (
            <>
              {conversations?.map((conversation) => (
                <ChatBanner
                  isSelected={conversation.id === props.selectedConversationId}
                  key={conversation.id}
                  {...conversation}
                  {...props}
                />
              ))}
            </>
          ) : (
            <div>No conversations</div>
          )}
        </List>
        <ConversationFab
          onClick={() => {
            setDialogOpen(true);
          }}
        />
      </StyledChatListContainer>

      <ConversationCreatorDialog
        createConversation={createConversation}
        open={dialogOpen}
        onClose={() => {
          setDialogOpen(false);
        }}
        users={
          authenThicationStore.users?.data
            ? authenThicationStore.users?.data
            : []
        }
      />
    </>
  );
}

export const ChatList = observer(ChatListComponent);
