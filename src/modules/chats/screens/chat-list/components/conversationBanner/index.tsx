import React from "react";
import { observer } from "mobx-react-lite";
import { Conversation } from "@/modules/chats/store";
import { TextContainer, StyledBanner } from "./styles";
import {
  Avatar,
  Typography,
  ListItemAvatar,
  ListItemText,
  Divider,
  Grid,
} from "@mui/material";
import { User } from "@/modules/authentications/store/store";

interface ChatBannerProps extends Conversation {
  userId: number;
  getUserDataById: (id: number) => User | undefined;
  onConversationClick?: (index: number) => void;
  isSelected: boolean;
}
function ChatBannerComponent({
  name,
  lastMessage,
  members,
  userId,
  getUserDataById,
  onConversationClick,
  id,
  isSelected,
}: ChatBannerProps) {
  return (
    <>
      <StyledBanner
        isSelected={isSelected}
        alignItems="flex-start"
        onClick={() => {
          if (onConversationClick) onConversationClick(id);
        }}
      >
        <ListItemAvatar>
          <Avatar />
        </ListItemAvatar>
        <ListItemText
          disableTypography
          primary={
            name ? name : members?.find((member) => member.id !== userId)?.name
          }
          secondary={
            lastMessage ? (
              <React.Fragment>
                <Grid container direction={"column"}>
                  <Grid item>
                    <Typography
                      sx={{ display: "inline" }}
                      variant="body2"
                      component="span"
                      color="text.primary"
                    >
                      {getUserDataById(lastMessage.userId)
                        ? getUserDataById(lastMessage.userId)?.name
                        : "User"}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography
                      sx={{ display: "inline" }}
                      component="span"
                      variant="body2"
                    >
                      {lastMessage?.text}
                    </Typography>
                  </Grid>
                </Grid>
              </React.Fragment>
            ) : (
              <TextContainer>No messages</TextContainer>
            )
          }
        />
      </StyledBanner>
      <Divider variant="inset" component="li" />
    </>
  );
}

export const ChatBanner = observer(ChatBannerComponent);
