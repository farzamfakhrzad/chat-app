import styled from "@emotion/styled";
import { ListItem } from "@mui/material";
export const TextContainer = styled.span`
  position: relative;
  height: 100%;
  width: 100%;
  padding-right: 20px;
  border-bottom: 1px solid rgba(0, 0, 0, 0.04);
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const StyledBanner = styled(ListItem, {
  shouldForwardProp: (prop) => prop !== "isSelected",
})<{ isSelected: boolean }>`
  background-color: ${(props) =>
    props.isSelected ? props.theme.palette.primary.main : "transparent"};
  border-radius: 10px;
`;
