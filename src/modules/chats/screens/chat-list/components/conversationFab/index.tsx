import AddIcon from "@mui/icons-material/Add";
import React from "react";
import { StyledFab } from "./styles";
export const ConversationFab = ({ onClick }: { onClick: () => void }) => {
  return (
    <StyledFab
      onClick={() => onClick()}
      size="small"
      color="secondary"
      aria-label="add"
    >
      <AddIcon />
    </StyledFab>
  );
};
