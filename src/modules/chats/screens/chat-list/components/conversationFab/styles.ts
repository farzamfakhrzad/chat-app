import styled from "@emotion/styled";
import { Fab } from "@mui/material";

export const StyledFab = styled(Fab)`
  position: absolute;
  bottom: 16px;
  right: 16px;
`;
