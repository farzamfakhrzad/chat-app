import { UserAvatar } from "@/components/UserAvatar";
import { User } from "@/modules/authentications/store/store";
import {
  Dialog,
  Button,
  DialogTitle,
  List,
  ListItem,
  ListItemButton,
  ListItemAvatar,
  ListItemIcon,
  Checkbox,
  DialogActions,
  TextField,
} from "@mui/material";
import React from "react";
interface ConversationCreatorDialogProps {
  onClose: () => void;
  open: boolean;
  users: User[];
  createConversation: (payload: { userIds: number[]; name?: string }) => void;
}
export const ConversationCreatorDialog = ({
  onClose,
  open,
  users,
  createConversation,
}: ConversationCreatorDialogProps) => {
  const [checked, setChecked] = React.useState([0]);
  const [groupName, setGroupName] = React.useState("");

  const handleToggle = (value: number) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };
  return (
    <Dialog onClose={onClose} open={open}>
      <DialogTitle>Selected Users to text</DialogTitle>
      <List sx={{ pt: 0, maxHeight: 200, overflow: "auto" }}>
        {users.map((user) => (
          <ListItem disableGutters key={user.id}>
            <ListItemButton>
              <ListItemIcon>
                <Checkbox
                  edge="end"
                  onChange={handleToggle(user.id)}
                  checked={checked.indexOf(user.id) !== -1}
                  tabIndex={-1}
                  disableRipple
                />
              </ListItemIcon>
              <ListItemAvatar>
                <UserAvatar username={user.name} />
              </ListItemAvatar>
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      {checked.length > 2 && (
        <TextField
          label="Group Name"
          value={groupName}
          onChange={(event) => {
            setGroupName(event.target.value);
          }}
        />
      )}
      <DialogActions>
        <Button
          variant="outlined"
          fullWidth
          onClick={() => {
            createConversation({
              userIds: checked.filter((value) => value !== 0),
              name: groupName,
            });
          }}
        >
          Create Conversation
        </Button>
      </DialogActions>
    </Dialog>
  );
};
