import { Message } from "..";

export const sortMessagesBasedOnTime = (messages: Message[]) => {
  function compareDateTime(a: Message, b: Message): number {
    const dateA = new Date(a.sentAt);
    const dateB = new Date(b.sentAt);
    return dateA.getTime() - dateB.getTime();
  }

  // Sort the array based on the date-time strings
  return messages.sort(compareDateTime);
};
