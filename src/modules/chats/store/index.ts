import { callEndpoint, RequestState, State } from "@/modules/axios";
import { reaction } from "mobx";
import { addDisposer, Instance, types } from "mobx-state-tree";
import { sortMessagesBasedOnTime } from "./helpers";

export interface CreateMessagePayload {
  text: string;
}
export interface CreateConversationPayload {
  user_ids: number[];
  name?: string;
}
export interface Message {
  userId: number;
  id: number;
  text: string;
  sentAt: string; //iso date
}
export interface Conversation {
  id: number;
  name: string;
  members?: User[];
  lastMessage?: Message;
}
export interface User {
  id: number;
  name: string;
  lastSeenAt: string; //iso date
}
export interface ArrayChats {
  id: number;
  name: string;
  lastSeenAt: string;
}
export const ChatStore = types
  .model("ChatStore", {
    selectedConversationId: types.optional(types.number, 0),
    selectedConversationMessages: types.optional(
      types.frozen<RequestState<Message[] | []>>(),
      {
        state: State.Initial,
      },
    ),
    selectedConversationData: types.optional(
      types.frozen<RequestState<Conversation>>(),
      {
        state: State.Initial,
      },
    ),
    loggedInUserId: types.optional(types.number, 0),
    chats: types.optional(types.frozen<RequestState<Conversation[]>>(), {
      state: State.Initial,
    }),
  })
  .actions((self) => ({
    setChats: (chats: RequestState<Conversation[]>) => {
      self.chats = chats;
    },
    setSelectedConversationMessages: (messages: RequestState<Message[]>) => {
      console.log(messages);
      let messagesPayload = messages;
      if (messages.data !== undefined && messages.data.length > 0) {
        messagesPayload = {
          data: sortMessagesBasedOnTime(messages.data),
          state: State.Success,
        };
      }
      self.selectedConversationMessages = messagesPayload;
    },
    setSelectedCobersationData: (
      conversationInfo: RequestState<Conversation>,
    ) => {
      self.selectedConversationData = conversationInfo;
    },
    setSelectedConversationId: (id: number) => {
      self.selectedConversationId = id;
    },
    setLoggedInUserId: (id: number) => {
      self.loggedInUserId = id;
    },
  }))

  .actions((self) => ({
    fetchConversation: async () => {
      const request = callEndpoint<Conversation[]>({
        config: {
          method: "GET",
          url: `/user/${self.loggedInUserId}/conversation`,
        },
        abort: self.chats.abort,
        store: self,
      });
      for await (const response of request) {
        self.setChats(response);
      }
    },

    createConversation: async (payload: CreateConversationPayload) => {
      const request = callEndpoint<Conversation>({
        config: {
          method: "POST",
          url: `/user/${self.loggedInUserId}/conversation`,
          data: payload,
        },
        abort: self.chats.abort,
        store: self,
      });
      const previousConversation = [
        ...(self.chats.data as Conversation[] | []),
      ];
      for await (const response of request) {
        if (response.state === State.Success) {
          previousConversation.unshift(response.data);
          self.setChats({
            state: State.Success,
            data: previousConversation,
          });
          self.setSelectedConversationId(response.data.id);
        } else if (response.state === State.Error) {
          self.setChats(response);
        }
      }
    },
    getConversationInfo: async () => {
      const request = callEndpoint<Conversation>({
        config: {
          method: "GET",
          url: `/user/${self.loggedInUserId}/conversation/${self.selectedConversationId}`,
        },
        store: self,
        abort: self.selectedConversationData.abort,
      });
      for await (const response of request) {
        self.setSelectedCobersationData(response);
      }
    },
    sendMesaage: async (payload: CreateMessagePayload) => {
      const request = callEndpoint<Message>({
        config: {
          method: "POST",
          url: `/user/${self.loggedInUserId}/conversation/${self.selectedConversationId}/message`,
          data: payload,
        },
        abort: self.selectedConversationMessages.abort,
      });
      const previousMessages = [
        ...(self.selectedConversationMessages.data as Message[] | []),
      ];
      for await (const response of request) {
        if (response.state === State.Success) {
          previousMessages.unshift(response.data);
          self.setSelectedConversationMessages({
            state: State.Success,
            data: previousMessages,
          });
          if (self.chats.state === State.Success) {
            const conversationData = [...self.chats.data] as Conversation[];
            const conversationIndex = conversationData.findIndex(
              (conversation) => conversation.id === self.selectedConversationId,
            );
            conversationData[conversationIndex].lastMessage = response.data;
            self.setChats({
              state: State.Success,
              data: conversationData,
            });
          }
        } else if (response.state === State.Error) {
          self.setSelectedConversationMessages(response);
        }
      }
    },
    fetchMessages: async () => {
      const request = callEndpoint<Message[]>({
        config: {
          method: "GET",
          url: `/user/${self.loggedInUserId}/conversation/${self.selectedConversationId}/message`,
        },
        abort: self.selectedConversationMessages.abort,
        store: self,
      });
      console.log(self.selectedConversationMessages);
      for await (const response of request) {
        self.setSelectedConversationMessages(response);
      }
    },
  }))
  .actions((self) => ({
    afterCreate: () => {
      addDisposer(
        self,
        reaction(
          () => [self.selectedConversationId],
          () => {
            if (self.selectedConversationId !== 0) {
              self.fetchMessages();
            }
          },
        ),
      );
    },
  }));
export type ChatStore = Instance<typeof ChatStore>;
