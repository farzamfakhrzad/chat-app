import { useContext, createContext } from "react";
import { ChatStore } from ".";

export const ChatStoreContext = createContext<ChatStore>(null!);
export function useChatStore(): ChatStore {
  return useContext(ChatStoreContext);
}
