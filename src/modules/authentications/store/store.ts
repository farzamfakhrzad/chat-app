import { callEndpoint, RequestState, State } from "@/modules/axios";
import { Instance, types } from "mobx-state-tree";

const MASTER_PASSWORD = process.env.VITE_MASTER_PASSWORD;

export interface User {
  id: number;
  name: string;
  lastSeenAt: string; //iso date
}

export const AuthenticationStore = types
  .model("AuthenticationStore", {
    LogegdInUser: types.optional(types.frozen<RequestState<User>>(), {
      state: State.Initial,
    }),
    users: types.optional(types.frozen<RequestState<User[]>>(), {
      state: State.Initial,
    }),
    ping: types.optional(types.frozen<RequestState<string>>(), {
      state: State.Initial,
    }),
  })
  .actions((self) => ({
    setLoggedInUser: (user: RequestState<User>) => {
      self.LogegdInUser = user;
    },
    setUsers: (users: RequestState<User[]>) => {
      self.users = users;
    },
  }))
  .views((self) => ({
    get isLoggedIn() {
      return self.LogegdInUser.state === State.Success;
    },
    get loginErrorMessage() {
      return self.LogegdInUser.state === State.Error
        ? self.LogegdInUser.error.code
        : null;
    },
  }))
  .actions((self) => ({
    searchUserById: (id: number) => {
      if (self.users.state !== State.Success)
        throw new Error("Users Should be fetched first");
      return self.users.data.find((user) => user.id === id);
    },
  }))
  .actions((self) => ({
    fetchUsers: async () => {
      const request = callEndpoint<User[]>({
        config: {
          method: "GET",
          url: "/user",
        },
        abort: self.users.abort,
      });
      for await (const response of request) {
        self.setUsers(response);
      }
    },
    getUserById: async (id: number) => {
      const request = callEndpoint<User>({
        config: {
          method: "GET",
          url: `/user/${id}`,
        },
        abort: self.LogegdInUser.abort,
        store: self,
      });
      for await (const response of request) {
        self.setLoggedInUser(response);
      }
    },
  }))
  .actions(() => ({
    rememberUser: (id: number) => {
      sessionStorage.setItem("userId", id.toString());
    },
  }))
  .actions((self) => ({
    attemptLogin: (name: string, password: string) => {
      if (self.users.state !== State.Success)
        throw new Error("Users Should be fetched first");
      const user = self.users.data?.find((user) => user.name === name);
      if (!user) {
        self.setLoggedInUser({
          state: State.Error,
          error: { code: "404", detail: "User not found" },
        });
        return;
      }
      if (password === MASTER_PASSWORD) {
        self.getUserById(user.id);
        self.rememberUser(user.id);
        return;
      }
      self.setLoggedInUser({
        state: State.Error,
        error: { code: "401", detail: "Wrong Password" },
      });
    },
  }))
  .actions((self) => ({
    init() {
      self.fetchUsers();
    },
  }));
export type AuthenticationStore = Instance<typeof AuthenticationStore>;
