import { useContext, createContext } from "react";
import { AuthenticationStore } from "./store";

export const AuthenticationStoreContext = createContext<AuthenticationStore>(
  null!,
);
export function useAuthenticationStore(): AuthenticationStore {
  return useContext(AuthenticationStoreContext);
}
