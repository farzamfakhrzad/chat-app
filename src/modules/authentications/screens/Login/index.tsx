import React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { SyntheticEvent, useEffect, useMemo } from "react";
import { useAuthenticationStore } from "@/modules/authentications/store/hooks";

import { useNavigate } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { useChatStore } from "@/modules/chats/store/hooks";
import { useGlobalStore } from "@/modules/app/hooks";
function LoginComponent() {
  const authenticationStore = useAuthenticationStore();
  const chatStore = useChatStore();
  const globalStore = useGlobalStore();
  const navigate = useNavigate();

  useEffect(() => {
    const errorCode = authenticationStore.loginErrorMessage;
    if (errorCode) {
      if (errorCode === "401") {
        globalStore.setNotification("Invalid username or password");
      }
    }
  }, [authenticationStore.LogegdInUser]);
  const isLoggedIn = useMemo(
    () => authenticationStore.isLoggedIn,
    [authenticationStore.isLoggedIn],
  );
  useEffect(() => {
    if (isLoggedIn && authenticationStore.LogegdInUser?.data?.id) {
      chatStore.setLoggedInUserId(authenticationStore.LogegdInUser.data.id);
      navigate("/");
    }
  }, [isLoggedIn]);

  const handleSubmit = (
    event: SyntheticEvent<HTMLFormElement, SubmitEvent>,
  ) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const loginPayload = {
      name: data.get("name"),
      password: data.get("password"),
    };
    if (!loginPayload.name || !loginPayload.password) {
      return;
    }
    authenticationStore.attemptLogin(
      loginPayload.name.toString(),
      loginPayload.password.toString(),
    );
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          Log in
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="name"
            label="Username"
            name="name"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
        </Box>
      </Box>
    </Container>
  );
}
export const Login = observer(LoginComponent);
