export const getValidAccessToken = () => {
  const accessToken = process.env.VITE_BUNQ_API_TOKEN;
  if (accessToken == null) {
    return undefined;
  }

  return accessToken;
};
