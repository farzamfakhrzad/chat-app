import axios, { AxiosRequestConfig, AxiosError, AxiosResponse } from "axios";
import { addDisposer, IAnyStateTreeNode } from "mobx-state-tree";
import { axiosInstance } from "./axios-instance";
import { LuggoError, RequestState, State } from ".";

function snakeToCamel(obj: any): any {
  if (obj === null || typeof obj !== "object") {
    return obj;
  }

  if (Array.isArray(obj)) {
    return obj.map((item) => snakeToCamel(item));
  }

  const camelObj: any = {};
  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      const newKey = key.replace(/_([a-z])/g, (_, group1) =>
        group1.toUpperCase(),
      );
      camelObj[newKey] = snakeToCamel(obj[key]);
    }
  }

  return camelObj;
}
type CustomAxiosError = AxiosError<{ errors?: LuggoError[] }>;
export function isAxiosError(error?: unknown): error is Omit<
  CustomAxiosError,
  "response"
> & {
  response: AxiosResponse<{ errors: LuggoError[] }>;
} {
  const errors = (error as CustomAxiosError | undefined)?.response?.data.errors;
  return errors != null && errors.length > 0;
}

export async function* callEndpoint<DataType>({
  abort,
  config,
  store,
}: {
  abort?: () => void;
  config: Omit<AxiosRequestConfig, "cancelToken">;
  store?: IAnyStateTreeNode;
}): AsyncGenerator<RequestState<DataType>> {
  if (abort != null) {
    abort();
  }
  const cancelToken = axios.CancelToken.source();
  yield { state: State.Loading, abort: cancelToken.cancel };
  if (store) {
    addDisposer(store, cancelToken.cancel);
  }

  try {
    const serverResponse = snakeToCamel(
      (
        await axiosInstance({
          ...config,
          cancelToken: cancelToken.token,
        })
      ).data,
    );
    const data =
      serverResponse.data !== undefined
        ? (serverResponse.data as DataType)
        : serverResponse;
    yield { state: State.Success as const, data };
  } catch (error) {
    let responseError: { state: State.Error; error: LuggoError } | undefined;
    if (axios.isCancel(error)) {
      yield { state: State.Initial };
    } else if (isAxiosError(error)) {
      responseError = {
        error: error.response.data.errors[0],
        state: State.Error as const,
      };
      if (responseError.error.code) {
        responseError = {
          error: {
            code: responseError.error.code,
            detail: "An unknown error occurred.",
          },
          state: State.Error,
        };
      }
    } else if (error instanceof Error) {
      responseError = {
        error: {
          code: "unknown",
          detail: "An unknown error occurred.",
        },
        state: State.Error,
      };
    } else {
      responseError = {
        state: State.Error,
        error: {
          code: "unknown",
          detail: "generic error",
        },
      };
    }
    if (responseError != null) {
      yield responseError;
    }
  }
}
