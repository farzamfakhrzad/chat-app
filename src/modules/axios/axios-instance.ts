import axios, { InternalAxiosRequestConfig } from "axios";
import { getValidAccessToken } from "./getValidAccessToken";

interface RequestHeaders {
  Authorization: string;
  [key: string]: string;
}
const axiosInstance = axios.create({
  headers: {
    Accept: "application/json, application/vnd.api+json",
    "Content-Type": "application/json",
  },
  baseURL: process.env.VITE_BUNQ_API_URL,
});

axiosInstance.interceptors.request.use(
  (request: InternalAxiosRequestConfig) => {
    let accessToken;
    try {
      accessToken = getValidAccessToken();
    } catch {
      accessToken = undefined;
    }
    if (accessToken != null) {
      (
        request.headers as RequestHeaders
      ).Authorization = `Bearer ${accessToken}`;
    }
    return request;
  },
);

export { axiosInstance };
