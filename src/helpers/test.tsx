import React from "react";
import "@testing-library/jest-dom";
import { render, RenderResult } from "@testing-library/react";
import { GlobalStore } from "@/modules/app/store";
import { GlobalStoreContext } from "@/modules/app/hooks";
import { AuthenticationStoreContext } from "@/modules/authentications/store/hooks";
import { AuthenticationStore } from "@/modules/authentications/store/store";
import { ChatStore } from "@/modules/chats/store";
import { ChatStoreContext } from "@/modules/chats/store/hooks";

function wrap(
  tree: React.ReactElement,
  options: {
    store?: boolean;
  } = {},
): RenderResult {
  function createdWrappedTree(toBeWrappedTree: React.ReactElement) {
    const { store } = options;
    let wrappedTree = toBeWrappedTree;

    if (store) {
      const authenticationStoreValue = AuthenticationStore.create({});
      authenticationStoreValue.init();
      const chatStoreValue = ChatStore.create({});
      const globalContextValue = GlobalStore.create({});
      wrappedTree = (
        <AuthenticationStoreContext.Provider value={authenticationStoreValue}>
          <GlobalStoreContext.Provider value={globalContextValue}>
            <ChatStoreContext.Provider value={chatStoreValue}>
              {wrappedTree}
            </ChatStoreContext.Provider>
          </GlobalStoreContext.Provider>
        </AuthenticationStoreContext.Provider>
      );
    }

    return wrappedTree;
  }

  const { rerender: originalRerender, ...originalRenderResult } = render(
    createdWrappedTree(tree),
  );

  return {
    ...originalRenderResult,
    rerender: (newTree) => originalRerender(createdWrappedTree(newTree)),
  };
}

/**
 * Mocks specified properties of a module and provide a `mock` function for each property listed in `properties`.
 *
 */
export function mockModule<Key extends string, Value>(
  module: { [key in Key]: Value },
  properties: Key[],
): ReturnTypeOfMockModule<Key, Value> {
  const originalValues = Object.fromEntries(
    properties.map((property) => [
      property,
      Reflect.getOwnPropertyDescriptor(module, property),
    ]),
  );

  afterEach(() => {
    properties.forEach((property) => {
      Reflect.defineProperty(
        module,
        property,
        originalValues[property] as PropertyDescriptor,
      );
    });
  });

  return Object.fromEntries(
    properties.map((property) => [
      property,
      {
        mock: (mockImplementation: Value): void => {
          // Some libraries export objects with readonly properties, overriding readonly properties is only possible
          // by `Reflect.defineProperty` and at the same time using `Reflect.defineProperty` does not cause any
          // problems for normal non-readonly properties, so we use it for all cases.
          Reflect.defineProperty(module, property, {
            get: () => mockImplementation,
            configurable: true,
          });
        },
      },
    ]),
  ) as ReturnTypeOfMockModule<Key, Value>;
}
type ReturnTypeOfMockModule<Key extends string, Value> = {
  [key in Key]: { mock: (mockImplementation: Value) => void };
};

export default wrap;
