import React, { useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import { CssBaseline } from "@mui/material";
import Routing from "./routes/Routing";
import { ChatStore } from "./modules/chats/store";
import { ChatStoreContext } from "./modules/chats/store/hooks";
import { AuthenticationStore } from "./modules/authentications/store/store";
import { AuthenticationStoreContext } from "./modules/authentications/store/hooks";
import { GlobalStoreContext } from "./modules/app/hooks";
import { GlobalStore } from "./modules/app/store";

const App = () => {
  const chatStore = ChatStore.create({});
  const authenticationStore = AuthenticationStore.create({});
  const globalStore = GlobalStore.create({});
  globalStore.init();

  return (
    <BrowserRouter>
      <CssBaseline />
      <GlobalStoreContext.Provider value={globalStore}>
        <ChatStoreContext.Provider value={chatStore}>
          <AuthenticationStoreContext.Provider value={authenticationStore}>
            <Routing />
          </AuthenticationStoreContext.Provider>
        </ChatStoreContext.Provider>
      </GlobalStoreContext.Provider>
    </BrowserRouter>
  );
};

export default App;
