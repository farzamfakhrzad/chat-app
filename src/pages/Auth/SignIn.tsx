import Container from "@mui/material/Container";
import React from "react";
import { Login } from "@/modules/authentications/screens/Login";
import { observer } from "mobx-react-lite";
function SignIn() {
  return (
    <Container component="main" maxWidth="xs">
      <Login />
    </Container>
  );
}

export default observer(SignIn);
