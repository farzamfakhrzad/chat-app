import React, { useEffect } from "react";
import { Container } from "@mui/material";
import { Messenger } from "../../modules/chats";
import { observer } from "mobx-react-lite";
import { useGlobalStore } from "@/modules/app/hooks";
const Home = () => {
  const globalStore = useGlobalStore();
  useEffect(() => {
    globalStore.setNotification(`Welcome`);
  });
  return (
    <Container sx={{ py: 2, position: "relative" }}>
      <Messenger />
    </Container>
  );
};

export default observer(Home);
