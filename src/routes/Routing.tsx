import React, { useEffect, useMemo } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { PathRouteProps } from "react-router";
import SignIn from "@/pages/Auth/SignIn";
import Home from "@/pages/Home/Home";
import { useAuthenticationStore } from "@/modules/authentications/store/hooks";
import { User } from "@/modules/authentications/store/store";
import { observer } from "mobx-react-lite";
import SimpleSnackbar from "@/components/SnackBar";
import { useGlobalStore } from "@/modules/app/hooks";

interface PrivateRouteProps extends PathRouteProps {
  user?: User;
}
const ProtectedRoute = ({ user, children }: PrivateRouteProps) => {
  if (!user) {
    return <Navigate to="/login" replace />;
  }

  return <>{children}</>;
};

const Routing = () => {
  const authenticationStore = useAuthenticationStore();
  const globalStore = useGlobalStore();

  const [appLoaded, setAppLoaded] = React.useState(false);

  const user = useMemo(
    () => authenticationStore.LogegdInUser?.data,
    [authenticationStore.LogegdInUser.state],
  );

  useEffect(() => {
    authenticationStore.init();
    setAppLoaded(true);
  }, []);

  useEffect(() => {
    setAppLoaded(globalStore.isConnectedToServer);
  }, [globalStore.isConnectedToServer]);
  return (
    <>
      {!appLoaded && <div>NOT CONNECTED TO SERVER</div>}
      <SimpleSnackbar
        handleClose={globalStore.resetNotification}
        message={globalStore.notification}
      />
      <Routes>
        <Route
          path="/"
          element={
            <ProtectedRoute user={user}>
              <Home />
            </ProtectedRoute>
          }
        />
        <Route path="/login" element={<SignIn />} />
      </Routes>
    </>
  );
};

export default observer(Routing);
