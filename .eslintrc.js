const path = require('path');

module.exports = {
  parser: '@babel/eslint-parser',
  plugins: ['babel', 'import', 'react-hooks', 'prettier'],
  ignorePatterns: ['!.eslintrc.js'],
  extends: [
    'plugin:react/recommended',
    'plugin:prettier/recommended',
  ],
  overrides: [
    {
        rules: {
            "import/extensions": "off",
            "@typescript-eslint/no-floating-promises": "off"
          },
      extends: [
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:eslint-comments/recommended',
      ],
      files: ['*.ts', '*.tsx'],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        ecmaFeatures: { jsx: true },
        ecmaVersion: '2019',
        project: ['tsconfig.json'],
        sourceType: 'module',
        warnOnUnsupportedTypeScriptVersion: false,
      },
    },
    {
      files: ['*.spec.ts', '*.spec.tsx', '*.snapshot.ts', '*.snapshot.tsx'],
    },
  ],
  env: {
    browser: true,
    es6: true,
    jest: true,
  },
 
};
