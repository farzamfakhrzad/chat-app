## Simple Chat App
Welcome to the Chat App! This is a simple and intuitive real-time chat application built using modern web technologies like React, TypeScript, and MUI. With Chat App.

## Login

use the USERNAME from the list of users provided in the users api or use the default username `ali` and the MASTER_PASSWORD  set in the .env file

## TODO List

- [ ] Add More Tests
- [ ] Add Storybook
- [ ] Fix small UI bugs
- [ ] Fix remaining eslint errors

 
## Paths

```
 resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
```

> tsconfig.json

```
"paths": {
      "@/*": ["./*"]
    }
```


### Scripts

| Script        | Description                        |
| ------------- | ---------------------------------- |
| yarn dev      | Runs the application.              |
| yarn build    | Create builds for the application. |
| yarn preview  | Runs the Vite preview              |
| yarn lint     | Display eslint errors              |
| yarn lint:fix | Fix the eslint errors              |
| yarn format   | Runs prettier for all files        |
| yarn test     | Run tests                          |

### Feature List

- [x] Login
- [x] Send Message
- [x] Receive Message
- [x] View Messages
- [x] View Users
- [x] View Conversations
- [x] Create Conversation
- [x] Create group Conversation
