
export default {
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    setupFilesAfterEnv: [
      "<rootDir>/src/helpers/jest.setup.ts"
    ],
    moduleNameMapper: {
      '^@/hooks(.*)$': '<rootDir>/src/hooks$1',
      '^@/assets(.*)(?<!.(svg|png|jpg|jpeg|mp4))$': '<rootDir>/src/assets$1',
      '^@/themes(.*)$': '<rootDir>/src/themes$1',
      '^@/components(.*)$': '<rootDir>/src/components$1',
      '^@/modules(.*)$': '<rootDir>/src/modules$1',
      '^@/helpers(.*)$': '<rootDir>/src/helpers$1',
      '^@/routes(.*)$': '<rootDir>/src/routes$1',
      '^@/pages(.*)$': '<rootDir>/src/pages$1',
      '^@/static(.*)$': '<rootDir>/static$1',
      '\\.(css|scss)$': 'identity-obj-proxy',
    },
    testMatch: [
      '<rootDir>/src/**/__tests__/**/*.{js,jsx,ts,tsx}',
      '<rootDir>/src/**/*.{spec,integration,snapshot,user-stories,regression}.{js,jsx,ts,tsx}',
    ],
  };