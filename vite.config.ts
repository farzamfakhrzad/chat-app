import react from '@vitejs/plugin-react-swc';
import { defineConfig } from 'vite';
import EnvironmentPlugin from "vite-plugin-environment"
import * as path from 'path';

export default defineConfig({
  plugins: [react(),EnvironmentPlugin("all")],
  build: {},
  resolve: {
    alias: {
      '@': path.resolve(__dirname, 'src'),
    },
  },
});
